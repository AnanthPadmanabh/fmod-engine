#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "FMODHeaders.h"

class MainContentComponent :  public Component,
                              public Slider::Listener,
                              public Timer
{
private:
	// FMOD objects
    EventSystem* eventsystem;
    Event* racingEngineEvent;
    
    // JUCE Objects
    Slider rpmSlider;
    Label rpmLabel;
    
public:
	MainContentComponent()
	:	eventsystem (nullptr),
        racingEngineEvent (nullptr),
        rpmLabel ("RPM", "RPM")
	{
        addAndMakeVisible (rpmSlider);
        addAndMakeVisible (rpmLabel);

        rpmLabel.setSize (60, 20);
        rpmLabel.attachToComponent (&rpmSlider, true);
        rpmSlider.setRange (0.0, 9000.0);
        rpmSlider.addListener (this);

        setSize (300, 100);
        
        initFMODEvent();
        startTimer (1000 / 60); // 60 fps
	}
    
    ~MainContentComponent()
    {
        shutdownFMODEvent();
    }
    
    void resized() override
    {
        rpmSlider.setBounds (10 + 60, 10, getWidth() - 20 - 60, 20);
    }
    
	void initFMODEvent()
	{
		// setup FMOD and load an FEV file
		ERRCHECK (EventSystem_Create (&eventsystem));
        
		// initialise FMOD and its event system
		ERRCHECK (eventsystem->init (64, FMOD_INIT_NORMAL, 0, FMOD_EVENT_INIT_NORMAL));
				
		// set the media path for the FEV and FSB files to be found
		ERRCHECK (eventsystem->setMediaPath (getResourcesPath().toUTF8()));
		
		// load an event file
		ERRCHECK (eventsystem->load ("RacingEngine.fev", 0, 0));
        
        // get the event and start it
        ERRCHECK (eventsystem->getEvent ("RacingEngine/root/RacingEngine", FMOD_EVENT_DEFAULT, &racingEngineEvent));
        ERRCHECK (racingEngineEvent->start());
	}
	
	void shutdownFMODEvent()
	{
        ERRCHECK (racingEngineEvent->stop());
		ERRCHECK (eventsystem->release());
		eventsystem = 0;
	}
    
    void sliderValueChanged (Slider* slider) override
    {
        EventParameter* param;
        
        if (&rpmSlider == slider)
        {
            ERRCHECK (racingEngineEvent->getParameter("rpm", &param));
        }
        else return;
        
        ERRCHECK (param->setValue ((float) slider->getValue()));
    }
	
	void timerCallback() override
	{
		if (eventsystem) // make sure we have an event system running
		{
			ERRCHECK (eventsystem->update());
		}
	}

};


#endif  // MAINCOMPONENT_H_INCLUDED
